FROM alpine:latest

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

RUN mkdir -p /home/app && \
    addgroup app && \
    adduser -D -G app app

WORKDIR /home/app
COPY es-rollover-controller .
RUN chown -R app:app /home/app
USER app
ENTRYPOINT ["./es-rollover-controller"]
