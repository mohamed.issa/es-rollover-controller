package esutil

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"

	log "github.com/sirupsen/logrus"

	"github.com/elastic/go-elasticsearch/v7/esapi"
	"github.com/pkg/errors"
)

// findLegacyIndexTemplates lists all legacy index templates from the elasticsearch api
func (es *ESClient) findLegacyIndexTemplates() ([]string, error) {
	searchPattern := fmt.Sprintf(es.config.FlagIndexTemplateFormat, "*")

	res, err := es.client.Indices.GetTemplate(es.client.Indices.GetTemplate.WithName(searchPattern))
	defer res.Body.Close()

	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response body: %s", res.String())
	}
	return parseLegacyIndexTemplates(body)
}

// parseLegacyIndexTemplates parses the list legacy index tempates api response to a slice of strings
func parseLegacyIndexTemplates(res []byte) ([]string, error) {

	var templateResp map[string]interface{}
	err := json.Unmarshal(res, &templateResp)
	if err != nil {
		return []string{}, err
	}

	indexTemplates := []string{}
	for name := range templateResp {
		indexTemplates = append(indexTemplates, name)
	}
	return indexTemplates, nil
}

// CleanupLegacyIndexTemplates deletes all legacy index templates matching the defined index template pattern
func (es *ESClient) CleanupLegacyIndexTemplates() error {

	legacyTemplates, err := es.findLegacyIndexTemplates()
	if err != nil {
		return errors.Wrapf(err, "finding legacy index templates")
	}

	if es.config.FlagCleanupLegacyIndexTemplates {
		for _, name := range legacyTemplates {
			res, err := esapi.IndicesDeleteTemplateRequest{Name: name}.Do(context.Background(), es.client.Transport)

			if res != nil {
				defer res.Body.Close()
			}

			if err != nil {
				return errors.Wrapf(err, "removing legacy index templates")
			}

			if res.IsError() {
				return errors.Wrapf(err, "removing legacy index templates")
			}
			log.Infof("removed legacy index template %s", name)
		}
	} else {
		log.Infof("would have cleaned up the following indices, but CLEANUP_LEGACY_INDEX_TEMPLATES is not set: %v", legacyTemplates)
	}

	return nil
}
